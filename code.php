<?php 
	
	class Product {

		public $name;
		public $price;
		public $description;
		public $category;
		public $stock;

		public function __construct($nameValue, $priceValue, $descriptionValue, $categoryValue, $stockValue){
			$this->name = $nameValue;
			$this->price = $priceValue;
			$this->description = $descriptionValue;
			$this->category = $categoryValue;
			$this->stock = $stockValue;
		}

		public function printDetails() {
			return "The product has a name of $this->name and its price is $this->price, and the stock no is $this->stock";
		}

		public function getPrice() {
			return $this->price;
		}

		public function getStock() {
			return $this->stock;
		}

		public function getCategory() {
			return $this->category;
		}

		public function setPrice($priceValue) {
			$this->price = $priceValue;
		}

		public function setStock($stockValue) {
			$this->stock = $stockValue;
		}

		public function setCategory($categoryValue) {
			$this->category = $categoryValue;
		}
	}

	class Mobile extends Product {

		public function printDetails() {
			return "Our latest mobile is $this->name with a cheapest price of $this->price";
		}
	}

	class Computer extends Product {

		public function printDetails() {
			return "Our newest computer is $this->name and its base price is $this->price";
		}
		
	}

	$xioamiMiMonitor = new Product('Xioami Mi Monitor', 22,000.00, 'Good for gaming and for coding', 5, 'computer-peripherals');
	$xioamiRedmiNote10Pro = new Mobile('Xioami Redmi Note 10 pro', 13590.00, 'Latest Xioami phone ever made', 10, 'mobiles and electronics');
	$HPBusinessLaptop = new Computer('HP Business Laptop', 29000.00, 'HP Laptop only made for business', 10, 'laptops and computer');

 ?>